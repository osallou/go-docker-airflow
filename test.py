from airflow import DAG
from airflow.configuration import conf
from airflow.executors.base_executor import BaseExecutor
from airflow.utils import State

from godockercli.auth import Auth
from godockercli.utils import Utils
from godockercli.httputils import HttpUtils

from datetime import datetime
import time
import os
import multiprocessing

from godocker_operator import GoDockerBashOperator

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2015, 1, 1),
    'email': ['olivier.sallou@irisa.fr'],
    'email_on_failure': False,
    'email_on_retry': False,
}

dag = DAG('testgodocker', default_args=default_args)

templated_command = """
    echo "{{ god_user }}"
    """

t1 = GoDockerBashOperator(
    task_id='print_date',
    bash_command=templated_command,
    cpu=2,
    params={'my_param': 'Paramater I passed in'},
    dag=dag)

#t1 = GoDockerBashOperator(
#    task_id='print_date',
#    bash_command='date',
#    dag=dag)

t2 = GoDockerBashOperator(
    task_id='sleep',
    email_on_failure=False,
    bash_command='sleep 5',
    dag=dag)

templated_command = """
{% for i in range(5) %}
    echo "{{ ds }}"
    echo "{{ macros.ds_add(ds, 7)}}"
    echo "{{ params.my_param }}"
{% endfor %}
"""

t3 = GoDockerBashOperator(
    task_id='templated',
    bash_command=templated_command,
    params={'my_param': 'Paramater I passed in'},
    dag=dag)

t2.set_upstream(t1)
t3.set_upstream(t1)
