from airflow.utils import State
from airflow.utils import apply_defaults
from airflow.models import Variable
#from airflow.operators.bash_operator import BashOperator
from airflow.models import BaseOperator

from godockercli.auth import Auth
from godockercli.utils import Utils
from godockercli.httputils import HttpUtils

from datetime import datetime
import time
import os
import multiprocessing
import logging
import json

class GoDockerBashOperator(BaseOperator):

    template_fields = ('bash_command',)
    template_ext = ('.sh', '.bash')
    ui_color = '#f0ede4'

    @apply_defaults
    def __init__(self, bash_command, cpu=1, ram=1, image='centos',
    volumes=None, is_root=False, env=None, *args, **kwargs):
        super(GoDockerBashOperator, self).__init__(*args, **kwargs)
        self.bash_command = bash_command
        self.env = env
        self.cpu = cpu
        self.ram = ram
        self.image = image
        self.volumes = volumes
        self.is_root = is_root

    def execute(self, context):
        try:
            Auth.authenticate()
            logging.info("Running %s", self.bash_command)
            command = "#!/bin/bash\n"+self.bash_command
            cpu = self.cpu
            if 'GOD_CPU' in os.environ:
                cpu = os.environ['GOD_CPU']
            ram = self.ram
            if 'GOD_RAM' in os.environ:
                ram = os.environ['GOD_RAM']
            image = self.image
            if 'GOD_IMAGE' in os.environ:
                image = os.environ['GOD_IMAGE']
            volumes = []
            if self.volumes is not None:
                volumes = self.volumes
            if 'GOD_VOLUMES' in os.environ:
                env_volumes = os.environ['GOD_VOLUMES'].split(',')
                for env_volume in env_volumes:
                    volume_details = env_volume.split(':')
                    volume = {'name': volume_details[0], 'acl': volume_details[1]}
            root = self.is_root
            if 'GOD_ROOT' in os.environ:
                env_root = os.environ['GOD_ROOT']
                if env_root == 1:
                    root = True

            login = Auth.login
            user_infos = Utils.get_userInfos(login)
            dt = datetime.now()

            job = {
                    'user' : {
                        'id' : user_infos['id'],
                        'uid' : user_infos['uid'],
                        'gid' : user_infos['gid']
                    },
                    'date': time.mktime(dt.timetuple()),
                    'meta': {
                        'name': self.task_id,
                        'description': 'airflow workflow',
                        'tags': ['airflow']
                    },
                    'requirements': {
                        'cpu': cpu,
                        # In Gb
                        'ram': ram,
                        'label': [],
                        'array': { 'values': None}
                    },
                    'container': {
                        'image': str(image),
                        'volumes': volumes,
                        'network': True,
                        'id': None,
                        'meta': None,
                        'stats': None,
                        'ports': [],
                        'root': root
                    },
                    'command': {
                        'interactive': False,
                        'cmd': command,
                    },
                    'status': {
                        'primary': None,
                        'secondary': None
                    }

                }
            result_submit = HttpUtils.http_post_request(
                    "/api/1.0/task", json.dumps(job),
                    Auth.server,
                    {'Authorization':'Bearer '+Auth.token, 'Content-type': 'application/json', 'Accept':'application/json'},
                    Auth.noCert
            )
            job_id = result_submit.json()['id']
            print(str(result_submit.json()['msg'])+". Job id is "+str(job_id))
            self.task_id = job_id
            not_over = True
            state = None
            while not_over:
                task = HttpUtils.http_get_request(
                        "/api/1.0/task/"+str(job_id),
                        Auth.server,
                        {'Authorization':'Bearer '+Auth.token},
                        Auth.noCert
                )

                job=task.json()
                if job['status']['primary'] == 'over':
                    if job['status']['secondary'] is None or not job['status']['secondary']:
                        state = State.SUCCESS
                    else:
                        state = State.FAILED
                    not_over = False
                time.sleep(1)
        except Exception as e:
                state = State.FAILED
                logging.error(str(e))
                # raise e
        if state != State.SUCCESS:
            raise Exception("Bash command failed")

    def on_kill(self):
        logging.info('Sending SIGTERM signal to bash process')
        HttpUtils.http_delete_request("/api/1.0/task/"+str(self.task_id), Auth.server, {'Authorization':'Bearer '+Auth.token}, Auth.noCert)
        self.sp.terminate()
